package ru.rumsd.javacore.chapter_9.nestedif;

/*
page 249 Пример вложенного интерфейса
 */
public class NestedIFDemo {
    public static void main(String[] args) {
        A.NestedIF nif = new B();

        if (nif.isNotNegative(10)) {
            System.out.println("10 неотрицаительное число");
        }
        if (nif.isNotNegative(-12)) {
            System.out.println("Не будет выведено");
        }
    }
}
