package ru.rumsd.javacore.chapter_9.nestedif;

class B implements A.NestedIF {
    @Override
    public boolean isNotNegative(int num) {
        return num < 0 ? false : true;
    }
}
