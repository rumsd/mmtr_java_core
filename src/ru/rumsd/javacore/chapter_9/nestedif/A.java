package ru.rumsd.javacore.chapter_9.nestedif;

class A {
    public interface NestedIF {
        boolean isNotNegative(int num);
    }
}
