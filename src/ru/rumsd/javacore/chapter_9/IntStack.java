package ru.rumsd.javacore.chapter_9;

/*
page 258 практический пример дефолтных методов в интерфейсе
 */

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public interface IntStack {
    void push(int item);

    int pop();

    default void clear() {
        throw new NotImplementedException();
    }
}
