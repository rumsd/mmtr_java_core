package ru.rumsd.javacore.chapter_9.p1;

/*
page 240-242 Пример защиты доступа
 */
public class Demo {
    public static void main(String[] args) {
        Protection p = new Protection();
        Derived d = new Derived();
        SamePackage s = new SamePackage();
    }
}
