package ru.rumsd.javacore.chapter_9.p1;

public class SamePackage {
    public SamePackage() {
        Protection p = new Protection();
        System.out.println();
        System.out.println("SamePackage cons");
        System.out.println("p.n = " + p.n);
        System.out.println("p.n_pro = " + p.n_pro);
        System.out.println("p.n_pub = " + p.n_pub);
    }
}
