package ru.rumsd.javacore.chapter_9.ifdefaultmethod;

/*
page 257-258 Реализация методов по умолчанию в интерфейсах
 */
public class DefaultMethodDemo {
    public static void main(String[] args) {
        MyIFImp obj = new MyIFImp();

        System.out.println(obj.getNumber());
        System.out.println(obj.getString());
    }
}
