package ru.rumsd.javacore.chapter_9.ifdefaultmethod;

public class MyIFImp implements MyIF {

    @Override
    public int getNumber() {
        return 100;
    }
}
