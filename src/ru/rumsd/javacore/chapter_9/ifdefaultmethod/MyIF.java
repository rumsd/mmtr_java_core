package ru.rumsd.javacore.chapter_9.ifdefaultmethod;

public interface MyIF {
    int getNumber();

    default String getString() {
        return "Строка возвращаемая по умолчанию";
    }
}
