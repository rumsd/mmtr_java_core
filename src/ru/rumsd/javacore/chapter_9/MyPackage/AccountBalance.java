package ru.rumsd.javacore.chapter_9.MyPackage;

class AccountBalance {
    public static void main(String[] args) {
        Balance[] current = {
                new Balance("Person 1", 123.3),
                new Balance("Person 2", 5000),
                new Balance("Person 3", -12),
        };

        for (Balance balance : current) {
            balance.show();
        }
    }
}
