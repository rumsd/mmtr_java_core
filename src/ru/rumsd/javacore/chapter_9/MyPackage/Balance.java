package ru.rumsd.javacore.chapter_9.MyPackage;

/*
page 237 Пример простого пакета
 */
public class Balance {
    String name;
    double balance;

    public Balance(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public void show() {
        if (balance < 0) {
            System.out.print("-->");
        }
        System.out.println(name + ": $" + balance);
    }
}
