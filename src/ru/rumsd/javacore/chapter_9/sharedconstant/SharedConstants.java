package ru.rumsd.javacore.chapter_9.sharedconstant;

public interface SharedConstants {
    int YES = 0;
    int NO = 1;
    int MAYBE = 2;
    int SOON = 3;
    int LATER = 4;
    int NEVER = 5;
}
