package ru.rumsd.javacore.chapter_9.p2;

import ru.rumsd.javacore.chapter_9.p1.Protection;

public class OuterPackage {
    public OuterPackage() {
        Protection p = new Protection();
        System.out.println();
        System.out.println("OuterPackage cons");
        System.out.println("n_pub = " + p.n_pub);
    }
}
