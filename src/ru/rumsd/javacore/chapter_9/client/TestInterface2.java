package ru.rumsd.javacore.chapter_9.client;

/*
page 247-248 Доступ к реализациям через ссылки на интерфейс
 */
public class TestInterface2 {
    public static void main(String[] args) {
        Callback c = new Client();
        AnotherClient client = new AnotherClient();

        c.callback(42);
        c = client;
        c.callback(42);
    }
}
