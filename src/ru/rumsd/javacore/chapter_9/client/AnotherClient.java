package ru.rumsd.javacore.chapter_9.client;

/*
page 247-248 Доступ к реализациям через ссылки на интерфейс
 */
public class AnotherClient implements Callback {

    @Override
    public void callback(int param) {
        System.out.println("param^2 = " + (param * param));
    }

}
