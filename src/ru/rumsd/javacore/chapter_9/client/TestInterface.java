package ru.rumsd.javacore.chapter_9.client;

/*
page 247-248 Доступ к реализациям через ссылки на интерфейс
 */
public class TestInterface {
    public static void main(String[] args) {
        Callback c = new Client();
        c.callback(42);
    }
}
