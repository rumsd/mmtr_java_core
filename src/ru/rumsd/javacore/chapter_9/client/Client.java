package ru.rumsd.javacore.chapter_9.client;

/*
page 246-247 Реализация интерфейсов
 */
public class Client implements Callback {
    @Override
    public void callback(int param) {
        System.out.println("callback(param) = " + param);
    }

    public void notIfaceMethod() {
        System.out.println("Метод не определенный в интерфейсе Callback");
    }
}
