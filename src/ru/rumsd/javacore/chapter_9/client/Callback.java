package ru.rumsd.javacore.chapter_9.client;

/*
page 246-247 Реализация интерфейсов
 */
public interface Callback {
    void callback(int param);
}
