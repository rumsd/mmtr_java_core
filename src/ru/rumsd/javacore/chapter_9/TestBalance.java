package ru.rumsd.javacore.chapter_9;

import ru.rumsd.javacore.chapter_9.MyPackage.Balance;

/*
page 244 Импорт пакетов
 */
public class TestBalance {
    public static void main(String[] args) {
        Balance balance = new Balance("Person1", 111.0);
        balance.show();
    }
}
