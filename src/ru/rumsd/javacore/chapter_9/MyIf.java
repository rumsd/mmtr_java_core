package ru.rumsd.javacore.chapter_9;

/*
page 261 реализация статических методов в интерфейсах
 */
public interface MyIf {
    int getNumber();

    default String getString() {
        return "default string";
    }

    static int getDefaultNumber() {
        return 0;
    }
}
