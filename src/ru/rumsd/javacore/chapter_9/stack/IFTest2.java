package ru.rumsd.javacore.chapter_9.stack;

/*
page 250-252 Применение интерфейсов
 */
public class IFTest2 {
    public static void main(String[] args) {
        DynStack dynStack1 = new DynStack(5);
        DynStack dynStack2 = new DynStack(5);

        for (int i = 0; i < 10; i++) {
            dynStack1.push(i);
        }

        for (int i = 0; i < 12; i++) {
            dynStack2.push(i);
        }

        System.out.println("dynStack1");
        for (int i = 0; i < 10; i++) {
            System.out.println(dynStack1.pop());
        }
        System.out.println("dynStack2");
        for (int i = 0; i < 12; i++) {
            System.out.println(dynStack2.pop());
        }
    }
}
