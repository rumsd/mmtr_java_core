package ru.rumsd.javacore.chapter_9.stack;

public class DynStack implements IntStack {
    private int[] values;
    private int tos;

    public DynStack(int len) {
        values = new int[len];
        tos = -1;
    }

    public void push(int item) {
        if (tos == values.length - 1) {
            int[] tmp = new int[values.length * 2];
            for (int i = 0; i < values.length; i++) {
                tmp[i] = values[i];
            }
            values = tmp;
            values[++tos] = item;
        } else {
            values[++tos] = item;
        }
    }

    public int pop() {
        if (tos == -1) {
            System.out.println("Стек пуст");
            return 0;
        }
        return values[tos--];
    }
}
