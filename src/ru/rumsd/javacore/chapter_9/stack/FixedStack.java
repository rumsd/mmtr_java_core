package ru.rumsd.javacore.chapter_9.stack;

public class FixedStack implements IntStack {
    private int[] values;
    private int tos;

    public FixedStack(int len) {
        values = new int[len];
        tos = -1;
    }

    public void push(int item) {
        if (tos == 9) {
            System.out.println("Стек заполнен");
        } else {
            values[++tos] = item;
        }
    }

    public int pop() {
        if (tos == -1) {
            System.out.println("Стек пуст");
            return 0;
        }
        return values[tos--];
    }
}
