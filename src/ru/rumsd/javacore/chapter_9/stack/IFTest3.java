package ru.rumsd.javacore.chapter_9.stack;

/*
page 250-252 Применение интерфейсов
 */
public class IFTest3 {
    public static void main(String[] args) {
        IntStack dynStack = new DynStack(5);
        IntStack fixStack = new FixedStack(8);

        for (int i = 0; i < 12; i++) {
            dynStack.push(i);
        }
        for (int i = 0; i < 8; i++) {
            fixStack.push(i);
        }

        System.out.println("dynStack");
        for (int i = 0; i < 12; i++) {
            System.out.println(dynStack.pop());
        }

        System.out.println("fixStack");
        for (int i = 0; i < 8; i++) {
            System.out.println(fixStack.pop());

        }
    }
}
