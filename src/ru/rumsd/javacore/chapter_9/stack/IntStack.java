package ru.rumsd.javacore.chapter_9.stack;

public interface IntStack {
    void push(int item);

    int pop();
}
