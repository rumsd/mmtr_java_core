package ru.rumsd.javacore.chapter_9.ifextend;

/*
page 255 Расщирение интерфейсов
 */
public class IFExtend {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();

        myClass.meth1();
        myClass.meth2();
        myClass.meth3();
    }
}
