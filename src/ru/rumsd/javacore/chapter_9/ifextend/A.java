package ru.rumsd.javacore.chapter_9.ifextend;

public interface A {
    void meth1();

    void meth2();
}
