package ru.rumsd.javacore.chapter_9.ifextend;

public class MyClass implements B {

    @Override
    public void meth1() {
        System.out.println("Релизация метода meth1()");
    }

    @Override
    public void meth2() {
        System.out.println("Релизация метода meth2()");
    }

    @Override
    public void meth3() {
        System.out.println("Релизация метода meth3()");
    }
}
