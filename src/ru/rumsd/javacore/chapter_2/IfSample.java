package ru.rumsd.javacore.chapter_2;

/*
page 67 Продемонстрировать применение условного оператора if
 */
public class IfSample {

    public static void main(String[] args) {
        int x, y;

        x = 10;
        y = 20;

        if (x < y) System.out.println("x меньше y");

        x = x * 2;
        if (x == y) System.out.println("x равно y");

        x = x * 2;
        if (x > y) System.out.println("x больше y");

        if (x == y) System.out.println("вы не увидите этого");
    }
}
