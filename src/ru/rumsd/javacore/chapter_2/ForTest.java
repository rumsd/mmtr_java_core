package ru.rumsd.javacore.chapter_2;

/*
page 68 Продемонстрировать применение цикла for.
 */
public class ForTest {
    public static void main(String[] args) {
        int x;

        for (x = 0; x < 10; x++) {
            System.out.println("Значение x: " + x);
        }

        System.out.println();

        for (x = 9; x >= 0; x--) {
            System.out.println("Значение x: " + x);
        }
    }
}
