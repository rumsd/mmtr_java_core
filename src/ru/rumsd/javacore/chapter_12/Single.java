package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 344 Одночленные аннотации
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MySingle {
    int value();
}

public class Single {

    @MySingle(100)
    public static void myMeth() {
        Single single = new Single();
        try {
            Class<?> c = single.getClass();
            Method method = c.getMethod("myMeth");
            MySingle anno = method.getAnnotation(MySingle.class);
            System.out.println(anno.value());

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        myMeth();
    }
}
