package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 340 получение всех аннотаций
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno2_1 {
    String str();

    int val();
}

@Retention(RetentionPolicy.RUNTIME)
@interface What {
    String description();
}

@What(description = "Аннотация тестового класса")
@MyAnno2_1(str = "Meta2_1", val = 99)
public class Meta2_1 {

    @What(description = "Аннотация тестового метода")
    @MyAnno2_1(str = "Testing", val = 100)
    public static void myMeth() {
        Meta2_1 meta = new Meta2_1();

        for (Annotation annotation : meta.getClass().getAnnotations()) {
            System.out.println(annotation);
        }

        try {
            Method method = meta.getClass().getMethod("myMeth");
            for (Annotation annotation : method.getAnnotations()) {
                System.out.println(annotation);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        myMeth();
    }
}
