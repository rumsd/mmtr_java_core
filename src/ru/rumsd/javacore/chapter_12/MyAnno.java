package ru.rumsd.javacore.chapter_12;

/*
page 334 простой тип аннотации
 */
public @interface MyAnno {
    String str();

    int val();
}
