package ru.rumsd.javacore.chapter_12;

/*
page 320 Воспользоваться встроенными в перечисление методами
 */

public class EnumDemo2 {
    public static void main(String[] args) {
        Apple ap;

        System.out.println("Константы перечисляемого типа Apple");

        for (Apple a : Apple.values()) {
            System.out.println(a);
        }
        System.out.println();

        ap = Apple.valueOf("WineSap");
        System.out.println("Переменная ap содержит " + ap);
    }
}
