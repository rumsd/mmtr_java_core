package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 337 Первый пример рефлексии
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno1 {
    String str();

    int val();
}

public class Meta {

    @MyAnno1(str = "пример аннотации", val = 100)
    public static void myMeth() {
        Meta meta = new Meta();
        try {
            Class<?> c = meta.getClass();
            Method method = c.getMethod("myMeth");
            MyAnno1 anno = method.getAnnotation(MyAnno1.class);
            System.out.println(anno.str() + " " + anno.val());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        myMeth();
    }
}
