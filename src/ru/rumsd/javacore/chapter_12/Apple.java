package ru.rumsd.javacore.chapter_12;

/*
page 319 Перечисление сортов яблок
 */
enum Apple {
    Jonathan, GoldenDel, RedDel, WineSap, Cortland
}
