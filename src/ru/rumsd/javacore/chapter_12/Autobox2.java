package ru.rumsd.javacore.chapter_12;

/*
page 329 Автоупаковка автораспаковка происходит при передаче параметров и возврате значений из методов
 */
public class Autobox2 {
    static int m(Integer i) {
        return i;
    }

    public static void main(String[] args) {
        Integer iObj = m(100);
        System.out.println(iObj);
    }
}
