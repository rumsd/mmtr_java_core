package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 343 Аннотации маркеры
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MyMarker {

}

public class Marker {

    @MyMarker
    public static void myMeth() {
        Marker marker = new Marker();
        try {
            Class<?> c = marker.getClass();
            Method method = c.getMethod("myMeth");

            if (method.isAnnotationPresent(MyMarker.class)) {
                System.out.println("MyMarker присутствует");
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        myMeth();
    }
}
