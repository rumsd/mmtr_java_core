package ru.rumsd.javacore.chapter_12;

/*
page 330 Автоупаковка/распаковка происходит в выражениях
 */
public class AutoBox3 {

    public static void main(String[] args) {
        Integer iObj, iObj2;
        int i;
        iObj = 100;

        System.out.println("iObj is " + iObj);
        System.out.println("++iObj is " + (++iObj));

        iObj2 = iObj + (iObj / 3);

        System.out.println("iObj after expression " + iObj2);

        i = iObj + (iObj / 3);
        System.out.println("i after expression " + i);
    }
}
