package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 343 Использование значений по умолчанию
 */

@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno3 {
    String str() default "Testing";

    int val() default 9000;
}

public class Meta3 {

    @MyAnno3()
    public static void myMeth() {
        Meta3 meta = new Meta3();
        try {
            Class<?> c = meta.getClass();
            Method method = c.getMethod("myMeth");
            MyAnno3 anno = method.getAnnotation(MyAnno3.class);
            System.out.println(anno.str() + " " + anno.val());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        myMeth();
    }
}

