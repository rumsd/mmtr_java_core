package ru.rumsd.javacore.chapter_12;

/*
page 329 Продемонстрировать автоупаковку автораспаковку
 */
public class AutoBox {
    public static void main(String[] args) {
        Integer iObj = 100;
        int i = iObj;
        System.out.println(iObj + " " + i);
    }
}
