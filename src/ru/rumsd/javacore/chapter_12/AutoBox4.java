package ru.rumsd.javacore.chapter_12;

/*
page 331 Автоупаковка при сложении разных типов
 */
public class AutoBox4 {
    public static void main(String[] args) {
        Integer iObj = 100;
        Double dObj = 98.6;
        dObj += iObj;
        System.out.println("dObj is " + dObj);
    }
}
