package ru.rumsd.javacore.chapter_12;

/*
page 321 enum с кострктором
 */
enum AppleWithConstructor {
    Jonathan(10), GoldenDel(9), RedDel(12), WineSap(15), Cortland(8);
    private int price;

    AppleWithConstructor(int price) {
        this.price = price;
    }

    int getPrice() {
        return price;
    }
}

public class EnumDemo3 {
    public static void main(String[] args) {
        System.out.println("WineSap costs " + AppleWithConstructor.WineSap.getPrice() + " c.");
        System.out.println();

        System.out.println("All price");
        for (AppleWithConstructor a : AppleWithConstructor.values()) {
            System.out.println(a + " costs " + a.getPrice() + " c.");
        }
        System.out.println();
    }
}
