package ru.rumsd.javacore.chapter_12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

/*
page 339 Второй пример рефлекссии
 */
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnno2 {
    String str();

    int val();
}

public class Meta2 {

    @MyAnno2(str = "Два параметра", val = 19)
    public static void myMeth(String str, int val) {
        Meta2 meta = new Meta2();

        try {
            Class<?> c = meta.getClass();
            Method method = c.getMethod("myMeth", String.class, int.class);
            MyAnno2 anno = method.getAnnotation(MyAnno2.class);
            System.out.println(anno.str() + " " + anno.val());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        myMeth("Тест", 10);
    }
}
