package ru.rumsd.javacore.chapter_14.hier3;

/*
page 426 Сравнение типов в обобщенной иерархии
 */
public class HierDemo3 {
    public static void main(String[] args) {
        Gen<Integer> iObj = new Gen<>(88);
        Gen2<Integer> iObj2 = new Gen2<>(99);
        Gen2<String> sObj2 = new Gen2<>("Текст сообщения");

        if (iObj2 instanceof Gen2<?>) {
            System.out.println("iObj2 instance of Ge2");
        }

        if (iObj2 instanceof Gen<?>) {
            System.out.println("iObj2 instance of Gen");
        }

        System.out.println();
        if (sObj2 instanceof Gen2<?>) {
            System.out.println("sObj2 instance of Gen2");
        }
        if (sObj2 instanceof Gen<?>) {
            System.out.println("sObj2 instance of Gen");
        }

        System.out.println();

        if (iObj instanceof Gen2<?>) {
            System.out.println("iObj instance of Gen2");
        }

        if (iObj instanceof Gen<?>) {
            System.out.println("iObj instance of Gen");
        }
    }
}
