package ru.rumsd.javacore.chapter_14.genoverride;

public class Gen<T> {
    T obj;

    public Gen(T obj) {
        this.obj = obj;
    }

    public T getObj() {
        return obj;
    }
}
