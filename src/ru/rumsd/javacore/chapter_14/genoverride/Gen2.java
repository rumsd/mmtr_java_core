package ru.rumsd.javacore.chapter_14.genoverride;

public class Gen2<T> extends Gen<T> {
    public Gen2(T obj) {
        super(obj);
    }

    @Override
    public T getObj() {
        System.out.println("getObj класса GetObj2");
        return obj;
    }
}
