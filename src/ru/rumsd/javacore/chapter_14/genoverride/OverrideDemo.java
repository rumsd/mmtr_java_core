package ru.rumsd.javacore.chapter_14.genoverride;

/*
page 428-429 Переопределение обобщенных методов
 */
public class OverrideDemo {
    public static void main(String[] args) {
        Gen<Integer> iObj = new Gen<>(88);
        Gen2<Integer> iObj2 = new Gen2<>(99);
        Gen2<String> sObj2 = new Gen2<>("Текс сообщения");

        System.out.println(iObj.getObj());
        System.out.println(iObj2.getObj());
        System.out.println(sObj2.getObj());
    }
}
