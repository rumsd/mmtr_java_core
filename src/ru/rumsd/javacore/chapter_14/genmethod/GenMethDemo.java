package ru.rumsd.javacore.chapter_14.genmethod;

/*
page 415 создание обобщенного метода
 */
public class GenMethDemo {
    static <T extends Comparable<T>, V extends T> boolean isIn(T x, V[] y) {
        for (int i = 0; i < y.length; i++) {
            if (x.equals(y[i])) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Integer[] nums = {1, 2, 3, 4, 5};
        if (isIn(2, nums)) {
            System.out.println("2 in nums");
        }

        if (!isIn(7, nums)) {
            System.out.println("7 not in nums");
        }

        String[] strings = {"one", "two", "tree", "four"};
        if (isIn("two", strings)) {
            System.out.println("two in strings");
        }
        if (!isIn("five", strings)) {
            System.out.println("five not in strings");
        }
    }
}
