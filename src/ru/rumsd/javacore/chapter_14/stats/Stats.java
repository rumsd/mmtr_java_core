package ru.rumsd.javacore.chapter_14.stats;

public class Stats<T extends Number> {
    T[] arr;

    public Stats(T[] arr) {
        this.arr = arr;
    }

    double average() {
        double sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i].doubleValue();
        }
        return sum / arr.length;
    }
}
