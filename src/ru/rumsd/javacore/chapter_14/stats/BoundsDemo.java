package ru.rumsd.javacore.chapter_14.stats;

/*
page 406 Ограниченные типы
 */
public class BoundsDemo {
    public static void main(String[] args) {
        Integer[] nums = {1, 2, 3, 4, 5};
        Stats<Integer> stats = new Stats<>(nums);
        System.out.println("stats int average = " + stats.average());

        Double[] dnums = {1.1, 2.2, 3.3, 4.4, 5.5};
        Stats<Double> doubleStats = new Stats<>(dnums);
        System.out.println("stats double average = " + doubleStats.average());
    }
}
