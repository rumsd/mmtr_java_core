package ru.rumsd.javacore.chapter_14.twogen;


public class TwoGen<T, V> {
    T objT;
    V objV;

    public TwoGen(T objT, V objV) {
        this.objT = objT;
        this.objV = objV;
    }

    public void showTypes() {
        System.out.println("Тип Т " + objT.getClass().getName());
        System.out.println("Тип V " + objV.getClass().getName());
    }

    public T getObjT() {
        return objT;
    }

    public V getObjV() {
        return objV;
    }
}
