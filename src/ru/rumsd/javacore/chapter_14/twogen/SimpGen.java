package ru.rumsd.javacore.chapter_14.twogen;

/*
page 403 Обобщенный класс с двумя параметрами
 */
public class SimpGen {
    public static void main(String[] args) {
        TwoGen<Integer, String> twoGen = new TwoGen<>(10, "Line");
        twoGen.showTypes();
        int v = twoGen.getObjT();
        String line = twoGen.getObjV();

        System.out.println("v = " + v);
        System.out.println("line = " + line);
    }
}
