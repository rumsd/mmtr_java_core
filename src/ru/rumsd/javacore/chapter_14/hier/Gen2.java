package ru.rumsd.javacore.chapter_14.hier;

public class Gen2<T, V> extends Gen<T> {
    V objv;

    public Gen2(T obj, V objv) {
        super(obj);
        this.objv = objv;
    }

    public V getObjv() {
        return objv;
    }
}
