package ru.rumsd.javacore.chapter_14.hier;

/*
page 423-424 Обобщенный супер класс
 */
public class HierDemo {
    public static void main(String[] args) {
        Gen2<String, Integer> x = new Gen2<>("Значение равно: ", 99);
        System.out.println(x.getObj() + x.getObjv());
    }
}
