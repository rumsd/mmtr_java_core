package ru.rumsd.javacore.chapter_14.boundedwildcard;

/*
page 412-413 ограниченные метасимвольные аргументы
 */
public class BoundedWildCard {

    static void showXY(Coords<?> c) {
        System.out.println("Координаты X, Y: ");
        for (int i = 0; i < c.coords.length; i++) {
            System.out.println("x = " + c.coords[i].x +
                    ", y = " + c.coords[i].y);
        }
        System.out.println();
    }

    static void showXYZ(Coords<? extends ThreeD> c) {
        System.out.println("Координаты X, Y, Z: ");
        for (int i = 0; i < c.coords.length; i++) {
            System.out.println(
                    "x = " + c.coords[i].x +
                            ", y = " + c.coords[i].y +
                            ", z = " + c.coords[i].z
            );
        }
        System.out.println();
    }

    static void showXYZT(Coords<? extends FourD> c) {
        System.out.println("Координаты X, Y, Z, T: ");
        for (int i = 0; i < c.coords.length; i++) {
            System.out.println(
                    "x = " + c.coords[i].x +
                            ", y = " + c.coords[i].y +
                            ", z = " + c.coords[i].z +
                            ", t = " + c.coords[i].t
            );
        }
        System.out.println();
    }

    public static void main(String[] args) {

        Coords<TwoD> twoDCoords = new Coords<>(new TwoD[]{
                new TwoD(0, 0),
                new TwoD(7, 9),
                new TwoD(18, 4),
                new TwoD(-1, -23)
        });
        showXY(twoDCoords);

        Coords<FourD> fourDCoords = new Coords<>(new FourD[]{
                new FourD(1, 2, 3, 4),
                new FourD(6, 8, 14, 8),
                new FourD(18, 4, 11, 22),
                new FourD(-1, -23, -8, 0)
        });
        showXY(fourDCoords);
        showXYZ(fourDCoords);
        showXYZT(fourDCoords);
    }
}
