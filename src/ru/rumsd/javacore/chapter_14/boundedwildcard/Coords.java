package ru.rumsd.javacore.chapter_14.boundedwildcard;

public class Coords<T extends TwoD> {
    T[] coords;

    public Coords(T[] coords) {
        this.coords = coords;
    }
}
