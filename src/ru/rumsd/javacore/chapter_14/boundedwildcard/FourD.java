package ru.rumsd.javacore.chapter_14.boundedwildcard;

public class FourD extends ThreeD {
    int t;

    public FourD(int x, int y, int z, int t) {
        super(x, y, z);
        this.t = t;
    }
}
