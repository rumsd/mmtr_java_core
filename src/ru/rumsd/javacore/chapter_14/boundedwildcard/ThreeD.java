package ru.rumsd.javacore.chapter_14.boundedwildcard;

public class ThreeD extends TwoD {
    int z;

    public ThreeD(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }
}
