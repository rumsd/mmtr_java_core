package ru.rumsd.javacore.chapter_14.gen;

public class Gen<T> {
    T obj;

    public Gen(T obj) {
        this.obj = obj;
    }

    public T getObj() {
        return obj;
    }

    public void show() {
        System.out.println("Типом Т является " + obj.getClass().getName());
    }
}
