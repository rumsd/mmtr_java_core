package ru.rumsd.javacore.chapter_14.gen;

/*
page 396-397 Простой пример обощения
 */
public class GenDemo {
    public static void main(String[] args) {
        Gen<Integer> genInt = new Gen<>(88);
        genInt.show();
        int v = genInt.getObj();
        System.out.println("v = " + v);

        Gen<String> genStr = new Gen<>("Текст");
        genStr.show();
        String value = genStr.getObj();
        System.out.println("value = " + value);
    }
}
