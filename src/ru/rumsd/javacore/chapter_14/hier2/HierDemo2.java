package ru.rumsd.javacore.chapter_14.hier2;

/*
page 424-425 обобщенный подкласс
 */
public class HierDemo2 {
    public static void main(String[] args) {
        Gen<String> w = new Gen<String>("Welcome ", 42);
        System.out.println(w.getObj() + w.getNum());
    }
}
