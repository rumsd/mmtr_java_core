package ru.rumsd.javacore.chapter_14.hier2;

public class NonGen {
    int num;

    public NonGen(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }
}
