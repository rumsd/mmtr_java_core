package ru.rumsd.javacore.chapter_14.hier2;

public class Gen<T> extends NonGen {
    T obj;

    public Gen(T obj, int num) {
        super(num);
        this.obj = obj;
    }

    public T getObj() {
        return obj;
    }
}
