package ru.rumsd.javacore.chapter_14.gencons;

/*
page 417 Обобщенные конструкторы
 */
public class GenConsDemo {
    public static void main(String[] args) {
        new GenCons(100).showVal();
        new GenCons(123.5f).showVal();
    }
}
