package ru.rumsd.javacore.chapter_14.gencons;

public class GenCons {
    private double val;

    public <T extends Number> GenCons(T val) {
        this.val = val.doubleValue();
    }

    public void showVal() {
        System.out.println("val: " + val);
    }
}