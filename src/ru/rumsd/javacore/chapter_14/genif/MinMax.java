package ru.rumsd.javacore.chapter_14.genif;

public interface MinMax<T extends Comparable<T>> {
    T min();

    T max();
}
