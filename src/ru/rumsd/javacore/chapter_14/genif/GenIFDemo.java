package ru.rumsd.javacore.chapter_14.genif;

/*
page 418 Обобщенные интерфейсы
 */
public class GenIFDemo {
    public static void main(String[] args) {
        MyClass<Integer> iobj = new MyClass<>(new Integer[]{1, 2, 3, 4, 5});
        MyClass<Character> cobj = new MyClass<>(new Character[]{'c', 'b', 'j', 'k', 'l'});

        System.out.println("iobj.min() = " + iobj.min());
        System.out.println("iobj.max() = " + iobj.max());
        System.out.println("cobj.min() = " + cobj.min());
        System.out.println("cobj.max() = " + cobj.max());
    }
}
