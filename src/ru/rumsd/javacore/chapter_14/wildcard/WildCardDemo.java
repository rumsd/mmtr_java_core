package ru.rumsd.javacore.chapter_14.wildcard;

/*
page 408-409 Применение метасимвольных аргументов
 */
public class WildCardDemo {
    public static void main(String[] args) {
        Stats<Integer> iobj = new Stats<>(new Integer[]{1, 2, 3, 4, 5});
        Stats<Double> dobj = new Stats<>(new Double[]{1.1, 2.2, 3.3, 4.4, 5.5});
        Stats<Float> fobj = new Stats<>(new Float[]{1.0f, 2.0f, 3.0f, 4.0f, 5.0f});

        System.out.println("iobj.average() = " + iobj.average());
        System.out.println("dobj.average() = " + dobj.average());
        System.out.println("fobj.average() = " + fobj.average());

        System.out.println("average iobj dobj");
        if (iobj.sameAvg(dobj)) {
            System.out.println("Равны");
        } else {
            System.out.println("Отличаются");
        }

        System.out.println("average iobj fobj");
        if (iobj.sameAvg(fobj)) {
            System.out.println("Равны");
        } else {
            System.out.println("Отличаются");
        }
    }
}
