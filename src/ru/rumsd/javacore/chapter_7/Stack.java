package ru.rumsd.javacore.chapter_7;

/*
page 191 stack 2.0
 */

public class Stack {
    private int[] values;
    private int tos;

    public Stack(int len) {
        values = new int[len];
        tos = -1;
    }

    public void push(int item) {
        if (tos == 9) {
            System.out.println("Стек заполнен");
        } else {
            values[++tos] = item;
        }
    }

    public int pop() {
        if (tos == -1) {
            System.out.println("Стек пуст");
            return 0;
        }
        return values[tos--];
    }
}

class TestStack {
    public static void main(String[] args) {
        Stack stack1 = new Stack(10);
        Stack stack2 = new Stack(10);

        for (int i = 0; i < 10; i++) {
            stack1.push(i);
        }

        for (int i = 10; i < 20; i++) {
            stack2.push(i);
        }

        System.out.println("stack1");
        for (int i = 0; i < 10; i++) {
            System.out.println(stack1.pop());
        }

        System.out.println("stack2");
        for (int i = 0; i < 10; i++) {
            System.out.println(stack2.pop());
        }

    }
}
