package ru.rumsd.javacore.chapter_7;

/*
page 181 Перегрузка констукторов
 */

public class Box {
    double width;
    double height;
    double depth;

    public Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box() {
        this.width = -1;
        this.height = -1;
        this.depth = -1;
    }

    public Box(double len) {
        width = height = depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

class OverloadCons {
    public static void main(String[] args) {
        Box box = new Box(10, 20, 15);
        Box box1 = new Box();
        Box box2 = new Box(7);

        System.out.println("box.volume() is" + box.volume());
        System.out.println("box1.volume() is" + box1.volume());
        System.out.println("box2.volume() is" + box2.volume());
    }
}