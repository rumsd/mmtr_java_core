package ru.rumsd.javacore.chapter_7;

/*
page 195 Длина массива
 */
public class Length {
    public static void main(String[] args) {
        int[] a1 = new int[10];
        int[] a2 = new int[]{1, 2, 3, 4, 5, 6, 99, -11};
        int[] a3 = {4, 3, 2, 1};
        System.out.println("a1[] length is " + a1.length);
        System.out.println("a2[] length is " + a2.length);
        System.out.println("a3[] length is " + a3.length);
    }
}
