package ru.rumsd.javacore.chapter_7;

/*
page 184 Аргументы примитивных типов передаются по значению
 */
class Test {
    void meth(int i, int j) {
        i *= 2;
        j /= 2;
    }
}

public class CallByValue {
    public static void main(String[] args) {
        Test obj = new Test();

        int i = 25, j = 20;

        System.out.println("i & j before call " + i + " " + j);

        obj.meth(i, j);

        System.out.println("i & j after call " + i + " " + j);

    }
}
