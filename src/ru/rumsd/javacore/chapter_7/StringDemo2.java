package ru.rumsd.javacore.chapter_7;

/*
page 201 Демонстрация методов класса String
 */
public class StringDemo2 {
    public static void main(String[] args) {
        String str1 = "first str";
        String str2 = "second str";
        String str3 = str1;

        System.out.println("len string " + str1.length());
        System.out.println("charAt(3) " + str2.charAt(3));

        String message = str1.equals(str2) ? "str1 == str2" : "str1 != str2";
        System.out.println(message);

        message = str1.equals(str3) ? "str1 == str3" : "str1 != str3";
        System.out.println(message);
    }
}
