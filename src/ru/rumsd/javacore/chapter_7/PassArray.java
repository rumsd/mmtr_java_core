package ru.rumsd.javacore.chapter_7;

/*
page 203 старых подход к аргументам переменной длины
 */
public class PassArray {
    static void vaTest(int[] args) {
        System.out.println("Колличество аргументов " + args.length);
        System.out.print("Содержимое: ");
        for (int arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] arguments1 = {10};
        int[] arguments2 = {1, 2, 3};
        int[] arguments3 = {};
        vaTest(arguments1);
        vaTest(arguments2);
        vaTest(arguments3);
    }
}
