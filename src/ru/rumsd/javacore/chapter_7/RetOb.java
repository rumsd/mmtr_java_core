package ru.rumsd.javacore.chapter_7;

/*
page 186 Возврат объектов
 */
class TestRet {
    int a;

    TestRet(int a) {
        this.a = a;
    }

    TestRet incrByTen() {
        return new TestRet(a + 10);
    }
}

public class RetOb {
    public static void main(String[] args) {
        TestRet obj1 = new TestRet(2);
        TestRet obj2;

        obj2 = obj1.incrByTen();

        System.out.println("obj1.a = " + obj1.a);
        System.out.println("obj2.a = " + obj2.a);

        obj2 = obj2.incrByTen();
        System.out.println("obj2.a = " + obj2.a);
    }
}
