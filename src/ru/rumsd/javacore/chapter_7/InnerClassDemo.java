package ru.rumsd.javacore.chapter_7;

/*
page 197 Продемонстрировать применение внутренного класса
 */
class Outer {
    int outer_x = 100;

    void test() {
        new Inner().display();
    }

    class Inner {
        void display() {
            System.out.println("outer_x = " + outer_x);
        }
    }
}

public class InnerClassDemo {
    public static void main(String[] args) {
        new Outer().test();
    }
}
