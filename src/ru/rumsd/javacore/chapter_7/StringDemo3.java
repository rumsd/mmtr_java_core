package ru.rumsd.javacore.chapter_7;

/*
page 202 массив строк
 */
public class StringDemo3 {
    public static void main(String[] args) {
        String[] strings = {"one", "two", "three"};
        for (int i = 0; i < strings.length; i++) {
            System.out.println("str[" + i + "] = " + strings[i]);
        }
    }
}
