package ru.rumsd.javacore.chapter_7;

/*
page 185 Передача объекта по ссылке
 */
class TestRef {
    int a, b;

    TestRef(int a, int b) {
        this.a = a;
        this.b = b;
    }

    void meth(TestRef o) {
        o.a *= 2;
        o.b /= 2;
    }
}

public class PassObjRe {
    public static void main(String[] args) {
        TestRef o = new TestRef(15, 20);
        System.out.println("o.a & o.b before call " + o.a + " " + o.b);

        o.meth(o);
        System.out.println("o.a & o.b after call " + o.a + " " + o.b);

    }
}
