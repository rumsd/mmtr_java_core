package ru.rumsd.javacore.chapter_7;

/*
page 206 перегузка методов с аргументами переменной длины
 */
public class VarArgs3 {

    static void vaTest(int... args) {
        System.out.println("vaTest(int ...)");
        System.out.println("Колличество аргументов " + args.length);
        System.out.print("Содержимое: ");
        for (int arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }

    static void vaTest(String msg, int... args) {
        System.out.println("vaTest(str, int...)");
        System.out.println(msg + args.length);
        System.out.print("Содержимое: ");
        for (int arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }

    static void vaTest(boolean... args) {
        System.out.println("vaTest(boolean ...)");
        System.out.println("Колличество аргументов " + args.length);
        for (boolean arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }


    public static void main(String[] args) {
        vaTest(1, 2, 3);
        System.out.println();
        vaTest("Проверка", 10, 20);
        System.out.println();
        vaTest(true, true, true);
    }
}
