package ru.rumsd.javacore.chapter_7;

/*
page 196 Stack array length
 */

public class StackLen {
    private int[] values;
    private int tos;

    public StackLen(int len) {
        values = new int[len];
        tos = -1;
    }

    public void push(int item) {
        if (tos == values.length - 1) {
            System.out.println("Стек заполнен");
        } else {
            values[++tos] = item;
        }
    }

    public int pop() {
        if (tos < 0) {
            System.out.println("Стек пуст");
            return 0;
        } else {
            return values[tos--];
        }
    }
}

class TestStack2 {

    public static void main(String[] args) {
        StackLen stack1 = new StackLen(6);
        StackLen stack2 = new StackLen(8);

        for (int i = 0; i < 6; i++) {
            stack1.push(i);
        }
        for (int i = 0; i < 8; i++) {
            stack2.push(i);
        }

        System.out.println("stack1");
        for (int i = 0; i < 6; i++) {
            System.out.println(stack1.pop());
        }

        System.out.println("stack2");
        for (int i = 0; i < 8; i++) {
            System.out.println(stack2.pop());
        }
    }
}
