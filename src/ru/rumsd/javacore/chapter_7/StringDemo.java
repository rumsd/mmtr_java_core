package ru.rumsd.javacore.chapter_7;

/*
page 200 продемонстрировать применение символьной строки
 */
public class StringDemo {
    public static void main(String[] args) {
        String str1 = "first str";
        String str2 = "second str";
        String str3 = str1 + " " + str2;

        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
    }
}
