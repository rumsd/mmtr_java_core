package ru.rumsd.javacore.chapter_7;
/*
page 190 Демонстрация модификаторов доступа private public
 */

class TestMod {
    int a;
    public int b;
    private int c;

    void setC(int c) {
        this.c = c;
    }

    int getC() {
        return c;
    }

}

public class AccessTest {
    public static void main(String[] args) {
        TestMod tm = new TestMod();
        tm.a = 10;
        tm.b = 11;
        tm.setC(12);

        System.out.println("tm.a = " + tm.a);
        System.out.println("tm.b = " + tm.b);
        System.out.println("tm.c = " + tm.getC());
    }
}
