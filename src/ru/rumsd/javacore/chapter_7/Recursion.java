package ru.rumsd.javacore.chapter_7;
/*
page 187 Простой пример рекурсии
 */

class Factorial {
    int fact(int num) {
        if (num == 1) return 1;
        return fact(num - 1) * num;
    }
}

public class Recursion {
    public static void main(String[] args) {
        Factorial f = new Factorial();

        System.out.println("fact(3) " + f.fact(3));
        System.out.println("fact(4) " + f.fact(4));
        System.out.println("fact(5) " + f.fact(5));
    }
}
