package ru.rumsd.javacore.chapter_7;

/*
page 188 Еще один пример рекурссии
 */
class RecTest {
    int[] values;

    public RecTest(int i) {
        values = new int[i];
    }

    void printArray(int i) {
        if (i == 0)
            return;
        else printArray(i - 1);
        System.out.println(String.format("[%d] - %d", i - 1, values[i - 1]));
    }
}

public class Recursion2 {
    public static void main(String[] args) {
        RecTest recTest = new RecTest(10);

        for (int i = 0; i < 10; i++) {
            recTest.values[i] = i;
        }
        recTest.printArray(10);
    }
}
