package ru.rumsd.javacore.chapter_7;

/*
page 199 Определить внутренний класс в цикле for
 */
class Outer2 {
    int outer_x = 100;

    void test() {
        for (int i = 0; i < 10; i++) {
            class Inner {
                void display() {
                    System.out.println("outer_x = " + outer_x);
                }
            }
            new Inner().display();
        }
    }
}

public class InnerClassDemo2 {
    public static void main(String[] args) {
        new Outer2().test();
    }
}
