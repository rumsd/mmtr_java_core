package ru.rumsd.javacore.chapter_7;

/*
page 205 аргументы переменной длины с обычными аргументами
 */
public class VarArgs2 {
    static void vaTest(String msg, int... args) {
        System.out.println(msg + args.length);
        System.out.print("Содержимое: ");
        for (int arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        vaTest("Один аргумент переменной длины ", 10);
        vaTest("Три аргумента переменной длины ", 10);
        vaTest("Без аргументов переменной длины ");
    }
}
