package ru.rumsd.javacore.chapter_7;

/*
page 178 Применить автоматическое преобразование типов к перегрузке
 */
public class OverloadDemo2 {

    void test() {
        System.out.println("without params");
    }

    void test(int a, int b) {
        System.out.println("a & b: " + a + " " + b);
    }

    void test(double a) {
        System.out.println("inside double method " + a);
    }
}

class Overload2 {
    public static void main(String[] args) {
        OverloadDemo2 od2 = new OverloadDemo2();
        int a = 88;

        od2.test();
        od2.test(10, 20);

        od2.test(a);
        od2.test(123.25);
    }
}
