package ru.rumsd.javacore.chapter_7;

/*
page 177 Продемонстрировать перегрузку методов
 */
public class OverloadDemo {

    void test() {
        System.out.println("without params");
    }

    void test(int a) {
        System.out.println("a: " + a);
    }

    void test(int a, int b) {
        System.out.println("a & b: " + a + " " + b);
    }

    double test(double a) {
        System.out.println("a: " + a);
        return a * a;
    }
}

class Overload {
    public static void main(String[] args) {
        OverloadDemo od = new OverloadDemo();
        double result;

        od.test();
        od.test(10);
        od.test(10, 20);
        result = od.test(123.25);
        System.out.println("od.test(123.25) is " + result);
    }
}
