package ru.rumsd.javacore.chapter_7;

/*
page 204 аргументы переменной длины
 */
public class VarArgs {
    static void vaTest(int... args) {
        System.out.println("Колличество аргументов " + args.length);
        System.out.print("Содержимое: ");
        for (int arg : args) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        vaTest(10);
        vaTest(1, 2, 3);
        vaTest();
    }
}
