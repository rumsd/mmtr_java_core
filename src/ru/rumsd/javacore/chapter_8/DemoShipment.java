package ru.rumsd.javacore.chapter_8;

/*
page 219 Расширение класса BoxWeiqht включением в него поля стоимости доставки
 */
class Shipment extends BoxWeight2 {
    double cost;

    public Shipment(Shipment shipment) {
        super(shipment);
        this.cost = shipment.cost;
    }

    public Shipment(double width, double height, double depth, double weight, double cost) {
        super(width, height, depth, weight);
        this.cost = cost;
    }

    public Shipment() {
        super();
        this.cost = -1;
    }

    public Shipment(double len, double weight, double cost) {
        super(len, weight);
        this.cost = cost;
    }
}

public class DemoShipment {
    public static void main(String[] args) {
        Shipment shipment1 = new Shipment(10, 20, 15, 10, 34.1);
        Shipment shipment2 = new Shipment(2, 3, 4, 0.76, 1.28);

        System.out.println("shipment1 volume = " + shipment1.volume());
        System.out.println("shipment1 weight = " + shipment1.weight);
        System.out.println("shipment1 cost = $" + shipment1.cost);
        System.out.println();

        System.out.println("shipment2 volume = " + shipment2.volume());
        System.out.println("shipment2 weight = " + shipment2.weight);
        System.out.println("shipment2 cost = $" + shipment2.cost);
        System.out.println();

    }
}
