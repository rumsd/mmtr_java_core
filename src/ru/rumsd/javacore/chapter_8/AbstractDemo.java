package ru.rumsd.javacore.chapter_8;

/*
page 229 Простой пример абстракции
 */
abstract class A6 {
    abstract void callme();

    void callmetoo() {
        System.out.println("Конкретный метод");
    }
}

class B6 extends A6 {

    void callme() {
        System.out.println("Класс B метод callme()");
    }
}

public class AbstractDemo {
    public static void main(String[] args) {
        B6 b = new B6();
        b.callme();
        b.callmetoo();
    }
}
