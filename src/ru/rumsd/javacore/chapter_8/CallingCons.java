package ru.rumsd.javacore.chapter_8;

/*
page 222 Продемонстрировать порядок вызова конструкторов
 */
class A3 {
    A3() {
        System.out.println("Cons A");
    }
}

class B3 extends A3 {
    B3() {
        System.out.println("Cons B");
    }
}

class C3 extends B3 {
    C3() {
        System.out.println("Cons C");
    }
}

public class CallingCons {
    public static void main(String[] args) {
        new C3();
    }
}
