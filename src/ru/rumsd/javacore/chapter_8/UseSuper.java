package ru.rumsd.javacore.chapter_8;

/*
page 218 предотвратить сокрытие имен
 */
class A2 {
    int i;
}

class B2 extends A2 {
    int i;

    public B2(int a, int b) {
        super.i = a;
        this.i = b;
    }

    void show() {
        System.out.println("parent.i = " + super.i);
        System.out.println("child.i = " + i);
    }
}

public class UseSuper {
    public static void main(String[] args) {
        new B2(2, 3).show();
    }
}
