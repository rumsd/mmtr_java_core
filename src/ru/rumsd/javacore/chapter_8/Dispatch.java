package ru.rumsd.javacore.chapter_8;

/*
page 225 Динамическая диспетчеризация методов
 */
class A5 {
    void callme() {
        System.out.println("class A5 method callme()");
    }
}

class B5 extends A5 {
    void callme() {
        System.out.println("class B5 method callme()");
    }
}

class C5 extends B5 {
    void callme() {
        System.out.println("class C5 method callme()");
    }
}

public class Dispatch {
    public static void main(String[] args) {
        A5 ref;
        ref = new A5();
        ref.callme();

        ref = new B5();
        ref.callme();

        ref = new C5();
        ref.callme();
    }
}
