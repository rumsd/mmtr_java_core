package ru.rumsd.javacore.chapter_8;

/*
page 216 полная реализация BoxWeight2
 */
class Box2 {
    private double width;
    private double height;
    private double depth;

    public Box2(Box2 bp) {
        this.width = bp.width;
        this.height = bp.height;
        this.depth = bp.depth;
    }

    public Box2(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box2() {
        this.width = -1;
        this.height = -1;
        this.depth = -1;
    }

    public Box2(double len) {
        width = height = depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

class BoxWeight2 extends Box2 {
    double weight;

    public BoxWeight2(BoxWeight2 bp) {
        super(bp);
        this.weight = bp.weight;
    }

    public BoxWeight2(double width, double height, double depth, double weight) {
        super(width, height, depth);
        this.weight = weight;
    }

    public BoxWeight2() {
        super();
        this.weight = -1;
    }

    public BoxWeight2(double len, double weight) {
        super(len);
        this.weight = weight;
    }
}

public class DemoSuper {
    public static void main(String[] args) {
        BoxWeight2 box1 = new BoxWeight2(10, 20, 15, 34.3);
        BoxWeight2 box2 = new BoxWeight2(2, 3, 4, 0.076);
        BoxWeight2 box3 = new BoxWeight2();
        BoxWeight2 cube = new BoxWeight2(3, 2);
        BoxWeight2 clonedBox = new BoxWeight2(box1);

        System.out.println("box1 volume = " + box1.volume());
        System.out.println("box1 weight = " + box1.weight);
        System.out.println();

        System.out.println("box2 volume = " + box2.volume());
        System.out.println("box2 weight = " + box2.weight);
        System.out.println();

        System.out.println("box3 volume = " + box3.volume());
        System.out.println("box3 weight = " + box3.weight);
        System.out.println();

        System.out.println("cube volume = " + cube.volume());
        System.out.println("cube weight = " + cube.weight);
        System.out.println();

        System.out.println("clonedBox volume = " + clonedBox.volume());
        System.out.println("clonedBox weight = " + clonedBox.weight);
        System.out.println();
    }
}
