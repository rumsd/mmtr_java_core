package ru.rumsd.javacore.chapter_8;

/*
page 209 Пример наследования
 */
class A {
    int i, j;

    public A(int i, int j) {
        this.i = i;
        this.j = j;
    }

    void showij() {
        System.out.println("i = " + i + ", j = " + j);
    }
}

class B extends A {
    int k;

    public B(int i, int j, int k) {
        super(i, j);
        this.k = k;
    }

    void showk() {
        System.out.println("k = " + k);
    }

    void sum() {
        System.out.println("sum k, i, j " + (i + k + j));
    }
}

public class SimpleInheritance {
    public static void main(String[] args) {
        A parent = new A(10, 20);
        B child = new B(7, 8, 9);
        System.out.println("Parent contains ");
        parent.showij();

        System.out.println("Child contains ");
        child.showij();
        child.showk();

        System.out.println("child sum");
        child.sum();
    }
}
