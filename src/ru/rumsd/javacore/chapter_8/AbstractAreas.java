package ru.rumsd.javacore.chapter_8;

/*
page 230 применение абстрактных классов и методов
 */

abstract class Figure2 {
    double dim1;
    double dim2;

    public Figure2(double dim1, double dim2) {
        this.dim1 = dim1;
        this.dim2 = dim2;
    }

    abstract double area();
}

class Rectangle2 extends Figure2 {
    public Rectangle2(double dim1, double dim2) {
        super(dim1, dim2);
    }

    double area() {
        System.out.println("Площадь четырех угольника: ");
        return dim1 * dim2;
    }
}

class Triangle2 extends Figure2 {

    public Triangle2(double dim1, double dim2) {
        super(dim1, dim2);
    }

    // площадь прямоугольного треугольника
    double area() {
        System.out.println("Площадь треугольника: ");
        return dim1 * dim2 / 2;
    }
}

public class AbstractAreas {
    public static void main(String[] args) {
        Rectangle2 rectangle = new Rectangle2(9, 5);
        Triangle2 triangle = new Triangle2(10, 8);
        Figure2 ref;

        ref = rectangle;
        System.out.println(ref.area());

        ref = triangle;
        System.out.println(ref.area());

    }
}
