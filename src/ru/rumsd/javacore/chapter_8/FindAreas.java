package ru.rumsd.javacore.chapter_8;

/*
page 227 полиморфизм во время выполнения
 */
class Figure {
    double dim1;
    double dim2;

    public Figure(double dim1, double dim2) {
        this.dim1 = dim1;
        this.dim2 = dim2;
    }

    double area() {
        System.out.println("Площадь фигуры не определена: ");
        return 0;
    }
}

class Rectangle extends Figure {
    public Rectangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    double area() {
        System.out.println("Площадь четырех угольника: ");
        return dim1 * dim2;
    }
}

class Triangle extends Figure {

    public Triangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    // площадь прямоугольного треугольника
    double area() {
        System.out.println("Площадь треугольника: ");
        return dim1 * dim2 / 2;
    }
}

public class FindAreas {
    public static void main(String[] args) {
        Figure figure = new Figure(10, 20);
        Rectangle rectangle = new Rectangle(9, 5);
        Triangle triangle = new Triangle(10, 8);
        Figure ref;

        ref = rectangle;
        System.out.println(ref.area());

        ref = triangle;
        System.out.println(ref.area());

        ref = figure;
        System.out.println(ref.area());
    }
}
