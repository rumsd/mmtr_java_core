package ru.rumsd.javacore.chapter_8;

/*
page 223 переопределение метода с вызовом метода из суперкласа
page 224 перегузка метода show
 */
class A4 {
    int i, j;

    public A4(int i, int j) {
        this.i = i;
        this.j = j;
    }

    void show() {
        System.out.print("i = " + i + " j = " + j + " ");
    }
}

class B4 extends A4 {
    int k;

    public B4(int i, int j, int k) {
        super(i, j);
        this.k = k;
    }

    void show() {
        super.show();
        System.out.println("k = " + k);
    }

    void show(String msg) {
        System.out.println(msg + k);
    }
}

public class Override {
    public static void main(String[] args) {
        B4 b = new B4(1, 2, 3);
        b.show("This is k ");
        b.show();
    }
}
