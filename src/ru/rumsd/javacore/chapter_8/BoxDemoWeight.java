package ru.rumsd.javacore.chapter_8;

/*
page 212 Расширение класса Box
*/

class Box {
    double width;
    double height;
    double depth;

    public Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Box() {
        this.width = -1;
        this.height = -1;
        this.depth = -1;
    }

    public Box(double len) {
        width = height = depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

class BoxWeight extends Box {
    double weight;

    public BoxWeight(double width, double height, double depth, double weight) {
        super(width, height, depth);
        this.weight = weight;
    }
}

class BoxColored extends Box {
    int c;

    public BoxColored(double width, double height, double depth, int c) {
        super(width, height, depth);
        this.c = c;
    }
}

public class BoxDemoWeight {
    public static void main(String[] args) {
        BoxWeight box1 = new BoxWeight(10, 20, 15, 34.3);
        BoxWeight box2 = new BoxWeight(2, 3, 4, 0.076);

        System.out.println("box1 volume = " + box1.volume());
        System.out.println("box1 weight = " + box1.weight);
        System.out.println();

        System.out.println("box2 volume = " + box2.volume());
        System.out.println("box2 weight = " + box2.weight);
        System.out.println();
    }
}
