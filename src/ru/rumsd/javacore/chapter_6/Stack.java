package ru.rumsd.javacore.chapter_6;

/*
page 174 Stack 10 целочисленных значений
 */
public class Stack {
    int[] stack = new int[10];
    int tos;

    public Stack() {
        this.tos = -1;
    }

    void pop(int item) {
        if (tos == 9) {
            System.out.println("Стек заполнен");
        } else {
            stack[++tos] = item;
        }
    }

    int pop() {
        if (tos < 0) {
            System.out.println("Стек не загружен");
            return 0;
        } else {
            return stack[tos--];
        }
    }
}

