package ru.rumsd.javacore.chapter_6;

public class BoxDemo {
    public static void main(String[] args) {
        Box box = new Box();
        box.width = 10;
        box.height = 20;
        box.depth = 15;

        System.out.println("vol is " + (box.width * box.height * box.depth));
    }
}
