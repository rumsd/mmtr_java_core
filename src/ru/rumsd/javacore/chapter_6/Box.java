package ru.rumsd.javacore.chapter_6;

/*
page 157 Программа, использующая класс Вох
 */
class Box {
    double width;
    double height;
    double depth;

    public Box() {
        System.out.println("Вызов конструктора");
        this.width = 10;
        this.height = 10;
        this.depth = 10;
    }

    public Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    void printVolume() {
        System.out.println("Volume is " + (width * height * depth));
    }

    double volume() {
        return width * height * depth;
    }

    void setDim(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}
