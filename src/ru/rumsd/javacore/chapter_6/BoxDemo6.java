package ru.rumsd.javacore.chapter_6;

/*
page 169 Box с конструктором
 */
public class BoxDemo6 {
    public static void main(String[] args) {
        Box box1 = new Box();
        Box box2 = new Box();

        System.out.println("Volume is " + box1.volume());
        System.out.println("Volume is " + box2.volume());
    }
}
