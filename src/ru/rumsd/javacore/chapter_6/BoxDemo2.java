package ru.rumsd.javacore.chapter_6;

/*
page 158 В этой программе объявляются два объекта класса Вох
 */

public class BoxDemo2 {
    public static void main(String[] args) {
        Box box1 = new Box();
        Box box2 = new Box();


        box1.width = 10;
        box1.height = 20;
        box1.depth = 15;

        box2.width = 3;
        box2.height = 6;
        box2.depth = 9;

        System.out.println("Vol of box1 is " + (box1.width * box1.height * box1.depth));
        System.out.println("Vol of box2 is " + (box2.width * box2.height * box2.depth));
    }
}
