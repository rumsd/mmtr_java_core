package ru.rumsd.javacore.chapter_6;

/*
page 167 В этой программе применяется метод с параметрами
 */
public class BoxDemo5 {
    public static void main(String[] args) {
        Box box1 = new Box();
        Box box2 = new Box();
        box1.setDim(10, 25, 15);
        box2.setDim(3, 6, 9);
        System.out.println("Volume is " + box1.volume());
        System.out.println("Volume is " + box2.volume());
    }
}
