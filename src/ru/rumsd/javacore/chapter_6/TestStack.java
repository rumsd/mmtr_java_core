package ru.rumsd.javacore.chapter_6;

/*
page 175 TestStack 10 целочисленных значений
 */
public class TestStack {
    public static void main(String[] args) {
        Stack stack1 = new Stack();
        Stack stack2 = new Stack();
        for (int i = 0; i < 10; i++) {
            stack1.pop(i);
        }
        for (int i = 10; i < 20; i++) {
            stack2.pop(i);
        }

        System.out.println("stack1");
        for (int i = 0; i < 10; i++) {
            System.out.println(stack1.pop());
        }

        System.out.println();

        System.out.println("stack2");
        for (int i = 0; i < 10; i++) {
            System.out.println(stack2.pop());
        }
    }
}
