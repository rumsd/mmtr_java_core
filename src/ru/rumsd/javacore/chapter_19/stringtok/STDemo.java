package ru.rumsd.javacore.chapter_19.stringtok;

import java.util.StringTokenizer;

/*
page 652 Использование StringTokenizer
 */
public class STDemo {
    static String in = "name=name23;" +
            "author=author123;" +
            "production=product123;" +
            "copy=copy123;";


    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer(in, "=;");
        while (st.hasMoreTokens()) {
            String key = st.nextToken();
            String val = st.nextToken();
            System.out.println(key + ": " + val);
        }
    }
}
