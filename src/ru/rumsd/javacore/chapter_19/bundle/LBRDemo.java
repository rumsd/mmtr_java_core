package ru.rumsd.javacore.chapter_19.bundle;

import java.util.Locale;
import java.util.ResourceBundle;

/*
page 710 Использование комплексных ресуров
 */
public class LBRDemo {
    public static void main(String[] args) {
        ResourceBundle rb = ResourceBundle.getBundle("SampleRB");
        System.out.println("English version");
        System.out.println("Title: " + rb.getString("title"));
        System.out.println("StartText: " + rb.getString("StartText"));
        System.out.println("StopText: " + rb.getString("StopText"));

        rb = ResourceBundle.getBundle("SampleRB", Locale.GERMAN);
        System.out.println("Title: " + rb.getString("title"));
        System.out.println("StartText: " + rb.getString("StartText"));
        System.out.println("StopText: " + rb.getString("StopText"));
    }
}
