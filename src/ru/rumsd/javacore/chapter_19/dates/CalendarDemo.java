package ru.rumsd.javacore.chapter_19.dates;

import java.util.Calendar;

/*
page 664 Использование Calendar
 */
public class CalendarDemo {
    public static void main(String[] args) {
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Calendar calendar = Calendar.getInstance();
        System.out.println("Дата: ");
        System.out.println(calendar.get(Calendar.YEAR) +
                "-" + months[calendar.get(Calendar.MONTH)] +
                "-" + calendar.get(Calendar.DATE));
        System.out.println("Время: ");
        System.out.println(calendar.get(Calendar.HOUR) +
                ":" + calendar.get(Calendar.MINUTE) +
                ":" + calendar.get(Calendar.SECOND));

        calendar.set(Calendar.HOUR, 10);
        calendar.set(Calendar.MINUTE, 11);
        calendar.set(Calendar.SECOND, 12);
        System.out.println("Измененое Время: ");
        System.out.println(calendar.get(Calendar.HOUR) +
                ":" + calendar.get(Calendar.MINUTE) +
                ":" + calendar.get(Calendar.SECOND));

    }
}
