package ru.rumsd.javacore.chapter_19.dates;

import java.util.Date;

/*
page 661 использование Date
 */
public class DateDemo {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);
        System.out.println("from 1970-01-01 = " + date.getTime());
    }
}
