package ru.rumsd.javacore.chapter_19.optional;

import java.util.Optional;

/*
page 659 Применение Optional
 */
public class OptionalDemo {
    public static void main(String[] args) {
        Optional<String> noVal = Optional.empty();
        Optional<String> hasVal = Optional.of("ABCDEFG");

        if (noVal.isPresent()) {
            System.out.println("не выведется");
        } else {
            System.out.println("noVal isEmpty");
        }

        if (hasVal.isPresent()) {
            System.out.println(hasVal.get());
        }

        System.out.println(noVal.orElse("Строка по умолчанию"));
    }
}
