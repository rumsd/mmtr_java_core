package ru.rumsd.javacore.chapter_19.timer;

import java.util.Timer;

/*
page 678 демонстрация Timer TimerTask
 */
public class TTest {
    public static void main(String[] args) {
        MyTimerTask timerTask = new MyTimerTask();
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000, 5000);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
        timer.cancel();
    }
}
