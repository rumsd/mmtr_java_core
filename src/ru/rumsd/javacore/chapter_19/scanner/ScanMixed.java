package ru.rumsd.javacore.chapter_19.scanner;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*
page 703 Scanner для чтения разнотипных файлов
 */
public class ScanMixed {
    public static void main(String[] args) throws IOException {
        int i;
        double d;
        boolean b;
        String str;

        FileWriter writer = new FileWriter("test.txt");
        writer.write("Тестирование Scanner 10 12,2 один true два false");
        writer.close();
        FileReader reader = new FileReader("test.txt");
        Scanner scanner = new Scanner(reader);


        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                i = scanner.nextInt();
                System.out.println("int: " + i);
            } else if (scanner.hasNextDouble()) {
                d = scanner.nextDouble();
                System.out.println("double: " + d);
            } else if (scanner.hasNextBoolean()) {
                b = scanner.nextBoolean();
                System.out.println("boolean: " + b);
            } else {
                str = scanner.next();
                System.out.println("String: " + str);
            }
        }
        scanner.close();
    }
}
