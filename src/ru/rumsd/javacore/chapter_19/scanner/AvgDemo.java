package ru.rumsd.javacore.chapter_19.scanner;

import java.util.Scanner;

/*
page 700 вычисление среднего значение Scanner
 */
public class AvgDemo {
    public static void main(String[] args) {
        Scanner conin = new Scanner(System.in);
        int count = 0;
        double sum = 0;

        while (conin.hasNext()) {
            if (conin.hasNextDouble()) {
                sum += conin.nextDouble();
                count++;
            } else {
                String str = conin.next();
                if (str.equals("exit")) break;
                else {
                    System.out.println("Ошибка данных");
                    return;
                }
            }
        }
        conin.close();
        System.out.println("Average is " + sum / count);
    }
}
