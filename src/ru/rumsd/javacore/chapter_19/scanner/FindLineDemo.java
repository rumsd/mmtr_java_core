package ru.rumsd.javacore.chapter_19.scanner;

import java.util.Scanner;

/*
page 705 применение метода findInLine()
 */
public class FindLineDemo {
    public static void main(String[] args) {
        String data = "Name: Tom Age: 28 ID: 77";
        Scanner scanner = new Scanner(data);

        scanner.findInLine("Age:");
        if (scanner.hasNext()) {
            System.out.println(scanner.next());
        } else {
            System.out.println("Ошибка");
        }
    }
}
