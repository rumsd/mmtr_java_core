package ru.rumsd.javacore.chapter_19.scanner;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*
page 701 Искользование Scanner для считывания из файла
 */
public class AvgFile {
    public static void main(String[] args) throws IOException {

        FileWriter writer = new FileWriter("test.txt");
        writer.write("2 3,4 5 6 7 exit");
        writer.close();
        FileReader reader = new FileReader("test.txt");
        Scanner conin = new Scanner(reader);
        int count = 0;
        double sum = 0;

        while (conin.hasNext()) {
            if (conin.hasNextDouble()) {
                sum += conin.nextDouble();
                count++;
            } else {
                String str = conin.next();
                if (str.equals("exit")) break;
                else {
                    System.out.println("Ошибка данных");
                    return;
                }
            }
        }
        conin.close();
        System.out.println("Average is " + sum / count);
    }
}
