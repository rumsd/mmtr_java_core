package ru.rumsd.javacore.chapter_19.observer;

/*
page 674 Демонстрация наблюдения за объектами + несколько наблюдателей
 */
public class ObserverDemo {
    public static void main(String[] args) {
        BeindWatched observed = new BeindWatched();
        Watcher watcher = new Watcher();
        Watcher2 watcher2 = new Watcher2();
        observed.addObserver(watcher);
        observed.addObserver(watcher2);
        observed.counter(10);
    }
}
