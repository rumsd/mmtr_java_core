package ru.rumsd.javacore.chapter_19.observer;

import java.util.Observable;
import java.util.Observer;

public class Watcher implements Observer {
    @Override
    public void update(Observable obj, Object arg) {
        System.out.println("Метод update вызван count = " + (Integer) arg);
    }
}
