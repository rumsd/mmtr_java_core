package ru.rumsd.javacore.chapter_19.observer;

import java.util.Observable;
import java.util.Observer;

public class Watcher2 implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if ((Integer) arg == 0)
            System.out.println("Готово" + '\7');
    }
}
