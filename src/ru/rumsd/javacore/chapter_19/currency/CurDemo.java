package ru.rumsd.javacore.chapter_19.currency;

import java.util.Currency;
import java.util.Locale;

/*
page 679 Использование Currency
 */
public class CurDemo {
    public static void main(String[] args) {
        Currency c = Currency.getInstance(Locale.US);
        System.out.println("Символ: " + c.getSymbol());
        System.out.println("Дробная часть: " + c.getDefaultFractionDigits());
    }
}
