package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 689 спецификатор точности
 */
public class PrecisionDemo {
    public static void main(String[] args) {
        System.out.println(new Formatter().format("%.4f", 123.1234567));
        System.out.println(new Formatter().format("%16.2e", 123.1234567));
        System.out.println(new Formatter().format("%.15s", "Форматировать в Java теперь очень просто"));
    }
}
