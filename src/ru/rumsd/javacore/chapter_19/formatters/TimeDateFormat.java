package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Calendar;
import java.util.Formatter;

/*
page 686 форматирование даты
 */
public class TimeDateFormat {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        System.out.println(new Formatter().format("%tc", cal));
        System.out.println(new Formatter().format("%tl:%tM", cal, cal));
        System.out.println(new Formatter().format("%tB %tb %tM", cal, cal, cal));

    }
}
