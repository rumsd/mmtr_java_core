package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 691 Пробел как спецификатор формата
 */
public class FormatDemo5 {
    public static void main(String[] args) {
        System.out.println(new Formatter().format("% d", -100));
        System.out.println(new Formatter().format("% d", 100));
        System.out.println(new Formatter().format("% d", -200));
        System.out.println(new Formatter().format("% d", 200));
    }
}
