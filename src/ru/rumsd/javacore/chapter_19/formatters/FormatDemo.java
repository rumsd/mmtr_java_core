package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 683 Пример форматирования строки Formatter
 */
public class FormatDemo {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        formatter.format("Строка %s, число %d, число с дробной частью %f", "aaaaa", 10, 22.11);
        System.out.println(formatter);
        formatter.close();
    }
}
