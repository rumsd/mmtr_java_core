package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 687 Использование %% %n
 */
public class FormatDemo3 {
    public static void main(String[] args) {
        Formatter formatter = new Formatter();
        formatter.format("это %% а это перенос%nстроки ну и число %d", 88);
        System.out.println(formatter);
        formatter.close();
    }
}
