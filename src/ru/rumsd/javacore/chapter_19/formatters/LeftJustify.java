package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 690 Выравнивание текста
 */
public class LeftJustify {
    public static void main(String[] args) {
        System.out.println(new Formatter().format("|%10.2f|", 123.123));
        System.out.println(new Formatter().format("|%-10.2f|", 123.123));
    }
}
