package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Calendar;
import java.util.Formatter;

/*
page 694 Использование относительных индексов
 */
public class FormatDemo6 {
    public static void main(String[] args) {
        System.out.println(new Formatter().format("Today is day %te of %<tB, %<tY", Calendar.getInstance()));
    }
}
