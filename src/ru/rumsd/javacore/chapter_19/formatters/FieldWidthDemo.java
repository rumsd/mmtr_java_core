package ru.rumsd.javacore.chapter_19.formatters;

import java.util.Formatter;

/*
page 688 вывести таблицу квадратов
 */
public class FieldWidthDemo {
    public static void main(String[] args) {
        Formatter formatter;
        for (int i = 1; i <= 10; i++) {
            formatter = new Formatter();
            formatter.format("%d %d %d", i, i * i, i * i * i);
            System.out.println(formatter);
            formatter.close();
        }
    }
}
