package ru.rumsd.javacore.chapter_3;

/*
page 81 Продемонстрировать применение значений типа Ьoolean
 */
public class BoolTest {
    public static void main(String[] args) {
        boolean b = false;
        System.out.println("b = " + b);
        b = true;
        System.out.println("b = " + b);

        if (b) System.out.println("Код выполняется");
        b = false;
        if (b) System.out.println("Код не выполняется");

        System.out.println("(10 > 9)" + (10 > 9));
    }
}
