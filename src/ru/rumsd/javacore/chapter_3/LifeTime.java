package ru.rumsd.javacore.chapter_3;

/*
page 89 Продемонстрировать срок действия переменной
 */

public class LifeTime {
    public static void main(String[] args) {
        int x;
        for (x = 0; x < 3; x++) {
            int y = -1;
            System.out.println("y = " + y);
            y = 100;
            System.out.println("now y is " + y);
        }
    }
}
