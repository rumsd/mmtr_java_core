package ru.rumsd.javacore.chapter_3;

/*
page 91 Продемонстрировать приведение типов
 */
public class Conversion {
    public static void main(String[] args) {
        int i = 257;
        double d = 323.142;

        System.out.println("int to byte");
        System.out.println(String.format("i & b = %d %d", i, (byte) i));

        System.out.println("double to int");
        System.out.println(String.format("d & i = %.3f %d", d, (int) d));

        System.out.println("double to byte");
        System.out.println(String.format("d & b = %.3f %d", d, (byte) d));
    }
}
