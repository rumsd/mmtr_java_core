package ru.rumsd.javacore.chapter_3;

/*
page 100 инициализирвать двумерный массив
 */
public class Matrix {
    public static void main(String[] args) {
        double[][] m = {
                {0 * 0, 1 * 0, 2 * 0, 3 * 0},
                {0 * 1, 1 * 1, 2 * 1, 3 * 1},
                {0 * 2, 1 * 2, 2 * 2, 3 * 2},
                {0 * 3, 1 * 3, 2 * 3, 3 * 3}
        };

        for (double[] line : m) {
            for (double d : line) {
                System.out.print(d + "  ");
            }
            System.out.println();
        }
    }
}
