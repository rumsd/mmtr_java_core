package ru.rumsd.javacore.chapter_3;

/*
page 96 Усовершенствованная версия предыдущей программы
 */
public class AutoArray {
    public static void main(String[] args) {
        int[] month_days = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 30};
        System.out.println(month_days[3] + " days in April");
    }
}
