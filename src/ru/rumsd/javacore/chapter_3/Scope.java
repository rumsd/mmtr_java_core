package ru.rumsd.javacore.chapter_3;

// page 88 Продемонстрировать область действия блока кода
public class Scope {
    public static void main(String[] args) {
        int x = 10;

        if (x == 10) {
            int y = 20;

            System.out.println(String.format("x & y: %d %d", x, y));
            x = y * 2;
        }
        System.out.println("x = " + x);
    }
}
