package ru.rumsd.javacore.chapter_3;

/*
page 79 Вычислить площадь круга
 */
public class Area {
    public static void main(String[] args) {
        double pi = 3.1416, r = 10.8, a;
        a = 2 * pi * r;
        System.out.println("Площадь круга равна " + a);
    }
}
