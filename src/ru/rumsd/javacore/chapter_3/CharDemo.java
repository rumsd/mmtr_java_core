package ru.rumsd.javacore.chapter_3;

/*
page 80 Продемонстрировать применение типа данных char
 */
public class CharDemo {
    public static void main(String[] args) {
        char ch1 = 88;
        char ch2 = 'Y';
        System.out.println(String.format("ch1 и ch2: %c %c", ch1, ch2));
    }
}
