package ru.rumsd.javacore.chapter_3;

/*
page 96 Вычисление среднего из массива значений
 */
public class Average {
    public static void main(String[] args) {
        double[] nums = {10.1, 10.2, 10.3, 10.4, 10.5};
        double result = 0;
        for (int i = 0; i < 5; i++) {
            result += nums[i];
        }
        System.out.println("average = " + result / 5);
    }
}
