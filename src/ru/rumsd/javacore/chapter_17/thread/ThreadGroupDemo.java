package ru.rumsd.javacore.chapter_17.thread;

/*
page 550-551 применение груп потоков
 */
public class ThreadGroupDemo {
    public static void main(String[] args) {
        ThreadGroup groupA = new ThreadGroup("Группа А");
        ThreadGroup groupB = new ThreadGroup("Группа Б");

        NewThread thread1 = new NewThread(groupA, "Один");
        NewThread thread2 = new NewThread(groupA, "Два");
        NewThread thread3 = new NewThread(groupB, "Три");
        NewThread thread4 = new NewThread(groupB, "Четыре");

        groupA.list();
        groupB.list();
        System.out.println();

        System.out.println("Прерывается группа А");
        Thread[] tga = new Thread[groupA.activeCount()];
        groupA.enumerate(tga);

        for (int i = 0; i < tga.length; i++) {
            ((NewThread) tga[i]).mysuspend();
        }

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            System.out.println("Главный поток прерван");
        }

        System.out.println("Восстановление группы А");

        for (int i = 0; i < tga.length; i++) {
            ((NewThread) tga[i]).myresume();
        }

        try {
            System.out.println("Ожидание завершения потоков исполнения");
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
        } catch (Exception e) {
            System.out.println("Исключение в главном потоке");
        }
        System.out.println("Главный поток завершен");
    }
}
