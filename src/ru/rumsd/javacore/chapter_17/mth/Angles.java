package ru.rumsd.javacore.chapter_17.mth;

/*
page 544 применение методов из Math
 */
public class Angles {
    public static void main(String[] args) {
        double theta = 120.0;
        System.out.println(theta + " градусов равно " + Math.toRadians(theta) + " радиан.");
        double rad = 1.312;
        System.out.println(rad + " радиан равно " + Math.toDegrees(rad) + " градусов.");
    }
}
