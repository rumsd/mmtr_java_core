package ru.rumsd.javacore.chapter_17.sys;

/*
page 532 Показать дирректорию пользователя
 */
public class ShowUserDir {
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
    }
}
