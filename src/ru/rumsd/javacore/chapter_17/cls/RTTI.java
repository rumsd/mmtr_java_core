package ru.rumsd.javacore.chapter_17.cls;

/*
page 538 Сведения о типе объекта во время выполнения
 */
public class RTTI {
    public static void main(String[] args) {
        X x = new X();
        Y y = new Y();
        Class<?> clazzX = x.getClass();
        System.out.println("x - объект типа: " + clazzX.getName());
        Class<?> clazzY = y.getClass();
        System.out.println("y - объект типа: " + clazzY.getName());
        System.out.println("Супер класс объекта y: " + clazzY.getSuperclass().getName());
    }
}
