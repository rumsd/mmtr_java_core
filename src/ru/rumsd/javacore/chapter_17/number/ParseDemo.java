package ru.rumsd.javacore.chapter_17.number;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
page 514 взаимное преобразование чисел и символьных строк
 */
public class ParseDemo {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        int num = 0;
        String line;
        while (!(line = reader.readLine()).equals("0")) {
            try {
                num = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                System.out.println("Неверный формат числа");
                continue;
            }
            sum += num;
        }
        System.out.println("sum = " + sum);
    }
}
