package ru.rumsd.javacore.chapter_17.number;

/*
page 514 преобразование по степени счисления
 */
public class StringConversion {
    public static void main(String[] args) {
        int num = 19648;
        System.out.println(num + " в двоичной форме " + Integer.toBinaryString(num));
        System.out.println(num + " в восьмиричной форме " + Integer.toOctalString(num));
        System.out.println(num + " в шестнадцатиричной форме " + Integer.toHexString(num));
    }
}
