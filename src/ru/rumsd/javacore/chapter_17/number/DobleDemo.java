package ru.rumsd.javacore.chapter_17.number;

/*
page 503 Пример создания Double
 */
public class DobleDemo {
    public static void main(String[] args) {
        Double d1 = new Double(3.14159);
        Double d2 = new Double("314159E-5");
        System.out.println(d1 + " = " + d2 + " = " + d1.equals(d2));
    }
}
