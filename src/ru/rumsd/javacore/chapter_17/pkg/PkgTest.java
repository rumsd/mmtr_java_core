package ru.rumsd.javacore.chapter_17.pkg;

/*
page 553 продемонстриовать применение класса Pakage
 */
public class PkgTest {
    public static void main(String[] args) {
        Package[] packages = Package.getPackages();
        for (Package pakage : packages) {
            System.out.println(pakage.getName());
            System.out.println(pakage.getImplementationTitle());
            System.out.println(pakage.getImplementationVendor());
            System.out.println(pakage.getImplementationVersion());
            System.out.println();
        }
    }
}
