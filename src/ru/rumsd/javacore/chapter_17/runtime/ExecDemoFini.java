package ru.rumsd.javacore.chapter_17.runtime;

/*
page 525 Ожидание завершения работы
 */
public class ExecDemoFini {
    public static void main(String[] args) {
        Runtime r = Runtime.getRuntime();
        Process p = null;
        try {
            p = r.exec("Notepad.exe");
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Notepad возвратил " + p.exitValue());
    }
}
