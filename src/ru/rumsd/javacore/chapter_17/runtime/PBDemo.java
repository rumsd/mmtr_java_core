package ru.rumsd.javacore.chapter_17.runtime;

import java.io.IOException;

/*
page 528 Запуск программы с аргументами
 */
public class PBDemo {
    public static void main(String[] args) {
        try {
            ProcessBuilder process = new ProcessBuilder("Notepad.exe", "C:\\java\\data.txt");
            process.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
