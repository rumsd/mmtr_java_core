package ru.rumsd.javacore.chapter_17.runtime;

/*
page 524-525 Выполнение сторонних программ
 */
public class ExecDemo {
    public static void main(String[] args) {
        Runtime r = Runtime.getRuntime();
        Process p = null;
        try {
            p = r.exec("Notepad.exe");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
