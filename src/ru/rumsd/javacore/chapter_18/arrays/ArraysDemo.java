package ru.rumsd.javacore.chapter_18.arrays;

import java.util.Arrays;

/*
page 633-634 Использование Arrays
 */
public class ArraysDemo {

    static void display(int[] arr) {
        for (int el : arr) {
            System.out.print(el + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            array[i] = -3 * i;
        }
        System.out.println("Исходный массив");
        display(array);

        System.out.println("Сортированный массив");
        Arrays.sort(array);
        display(array);

        System.out.println("Массив после вызова fill(): ");
        Arrays.fill(array, 2, 6, -1);
        display(array);

        System.out.println("Повторная сортировка массива");
        Arrays.sort(array);
        display(array);

        System.out.println("Двоичный поиск. Индекс '-9' = " + Arrays.binarySearch(array, -9));
    }
}
