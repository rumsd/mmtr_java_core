package ru.rumsd.javacore.chapter_18.comparator;

import java.util.Map;
import java.util.TreeMap;

/*
page 620-621 Использование thenComparing()
 */
public class TreeMapDemo2A {
    public static void main(String[] args) {
        TreeMap<String, Double> hm = new TreeMap<>(new CompLastNames().thenComparing(new CompFirstNames()));
        hm.put("APerson A", 123.11);
        hm.put("CPerson C", 111.11);
        hm.put("TPerson T", 222.11);
        hm.put("QPerson Q", 333.11);
        hm.put("LPerson L", 444.11);

        for (Map.Entry<String, Double> entry : hm.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println();
        double balance = hm.get("CPerson C");
        hm.put("CPerson C", balance + 1000);
        System.out.println("new balance Person 1 = " + hm.get("CPerson C"));
    }
}
