package ru.rumsd.javacore.chapter_18.comparator;

import java.util.Comparator;

public class CompLastNames implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
        int i, j;
        i = a.lastIndexOf(' ');
        j = b.lastIndexOf(' ');
        return a.substring(i).compareToIgnoreCase(b.substring(j));
    }
}
