package ru.rumsd.javacore.chapter_18.comparator;

import java.util.Comparator;

public class TComp implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
        int i, k, j;
        i = a.lastIndexOf(' ');
        j = b.lastIndexOf(' ');
        k = a.substring(i).compareTo(b.substring(j));
        if (k == 0) {
            return a.compareTo(b);
        } else {
            return k;
        }
    }
}
