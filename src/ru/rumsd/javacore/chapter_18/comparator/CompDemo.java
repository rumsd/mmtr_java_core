package ru.rumsd.javacore.chapter_18.comparator;

import java.util.TreeSet;

/*
page 617 Применение компаратора
 */
public class CompDemo {
    public static void main(String[] args) {
        TreeSet<String> ts = new TreeSet<>(new MyComp());
        ts.add("C");
        ts.add("A");
        ts.add("B");
        ts.add("E");
        ts.add("F");
        System.out.println("ts = " + ts);
    }
}
