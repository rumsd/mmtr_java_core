package ru.rumsd.javacore.chapter_18.comparator;

import java.util.TreeSet;

/*
page 618-619 Лямбда выражения для создания компаратора
 */
public class CompDemo2 {
    public static void main(String[] args) {
        TreeSet<String> ts = new TreeSet<>((a, b) -> b.compareTo(a));
        ts.add("C");
        ts.add("A");
        ts.add("B");
        ts.add("D");
        ts.add("E");
        ts.add("F");
        for (String el : ts) {
            System.out.print(el + " ");
        }
    }
}
