package ru.rumsd.javacore.chapter_18.comparator;

import java.util.Comparator;

public class CompFirstNames implements Comparator<String> {
    @Override
    public int compare(String a, String b) {
        return a.compareToIgnoreCase(b);
    }
}
