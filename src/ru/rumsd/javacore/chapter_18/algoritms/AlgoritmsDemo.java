package ru.rumsd.javacore.chapter_18.algoritms;

import java.util.Collections;
import java.util.LinkedList;

/*
page 628 Демонстрация алгоритмов Collection
 */
public class AlgoritmsDemo {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(-8);
        list.add(20);
        list.add(-20);
        list.add(8);
        Collections.sort(list, Collections.reverseOrder());
        System.out.println("list reverse " + list);
        System.out.println();

        Collections.shuffle(list);
        System.out.println("list suffle " + list);
        System.out.println();

        System.out.println("list min = " + Collections.min(list));
        System.out.println("list max = " + Collections.max(list));
    }
}
