package ru.rumsd.javacore.chapter_18.collection;

import java.util.Map;
import java.util.TreeMap;

/*
page 611-612 использование TreeMap
 */
public class ThreeMapDemo {
    public static void main(String[] args) {
        TreeMap<String, Double> hm = new TreeMap<>();
        hm.put("Person 1", 123.11);
        hm.put("Person 2", 111.11);
        hm.put("Person 3", 222.11);
        hm.put("Person 4", 333.11);
        hm.put("Person 5", 444.11);

        for (Map.Entry<String, Double> entry : hm.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        System.out.println();
        double balance = hm.get("Person 1");
        hm.put("Person 1", balance + 1000);
        System.out.println("new balance Person 1 = " + hm.get("Person 1"));
    }
}
