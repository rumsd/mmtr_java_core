package ru.rumsd.javacore.chapter_18.collection;

import java.util.TreeSet;

/*
page 588 применение класса TreeSet
 */
public class TheeSetDemo {
    public static void main(String[] args) {
        TreeSet<String> ts = new TreeSet<>();
        ts.add("C");
        ts.add("A");
        ts.add("B");
        ts.add("E");
        ts.add("F");
        ts.add("D");
        System.out.println(ts);
    }
}
