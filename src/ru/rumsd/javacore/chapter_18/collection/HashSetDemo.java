package ru.rumsd.javacore.chapter_18.collection;

import java.util.HashSet;

/*
page 586 применение HashSet
 */
public class HashSetDemo {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();

        set.add("Бета");
        set.add("Альфа");
        set.add("Эта");
        set.add("Гамма");
        set.add("Эпсилон");
        set.add("Омега");
        System.out.println(set);
    }
}
