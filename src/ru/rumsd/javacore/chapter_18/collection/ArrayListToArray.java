package ru.rumsd.javacore.chapter_18.collection;

import java.util.ArrayList;

/*
page 584 преобразование ArrayList в массив
 */
public class ArrayListToArray {

    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            arr.add(i);
        }

        System.out.println("arr = " + arr);
        Integer[] iarr = new Integer[arr.size()];
        iarr = arr.toArray(iarr);

        int sum = 0;
        for (int i : iarr) {
            sum += i;
        }
        System.out.println("sum = " + sum);
    }
}
