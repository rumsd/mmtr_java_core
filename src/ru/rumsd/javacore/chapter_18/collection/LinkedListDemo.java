package ru.rumsd.javacore.chapter_18.collection;

import java.util.LinkedList;

/*
page 584-585 демонстрация применения класса LinkedList
 */
public class LinkedListDemo {
    public static void main(String[] args) {
        LinkedList<String> ll = new LinkedList<>();
        ll.add("F");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.addLast("Z");
        ll.addFirst("A");
        ll.add(1, "A");
        System.out.println("ll before: " + ll);

        ll.remove("F");
        ll.remove(2);
        System.out.println("ll after delete elements" + ll);

        ll.removeFirst();
        ll.removeLast();
        System.out.println("ll after delete first/last" + ll);

        ll.set(2, ll.get(2) + "changed");
        System.out.println("ll after change " + ll);
    }
}
