package ru.rumsd.javacore.chapter_18.collection;

import java.util.LinkedList;

/*
page 599-600 Пример использования коллекций с созданным классом
 */
public class MailList {
    public static void main(String[] args) {
        LinkedList<Address> mails = new LinkedList<>();
        mails.add(new Address("person1", "street1", "city1", "state1", "code1"));
        mails.add(new Address("person2", "street2", "city2", "state2", "code2"));
        mails.add(new Address("person3", "street3", "city3", "state3", "code3"));

        for (Address mail : mails) {
            System.out.println(mail);
        }
    }
}
