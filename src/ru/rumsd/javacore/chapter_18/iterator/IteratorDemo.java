package ru.rumsd.javacore.chapter_18.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/*
page 594 применение итератора
 */
public class IteratorDemo {
    public static void main(String[] args) {
        ArrayList<String> arr = new ArrayList<>();
        arr.add("F");
        arr.add("B");
        arr.add("C");
        arr.add("D");
        arr.add("E");

        Iterator<String> iter = arr.iterator();

        System.out.println("arr");
        while (iter.hasNext()) {
            System.out.print(iter.next() + " ");
        }
        System.out.println();


        ListIterator<String> lIter = arr.listIterator();
        while (lIter.hasNext()) {
            String el = lIter.next();
            lIter.set(el + "+");
        }

        System.out.println("arr after change");
        iter = arr.iterator();
        while (iter.hasNext()) {
            System.out.print(iter.next() + " ");
        }
        System.out.println();

        System.out.println("arr reverse");
        while (lIter.hasPrevious()) {
            System.out.print(lIter.previous() + " ");
        }
        System.out.println();
    }
}
