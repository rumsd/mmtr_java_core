package ru.rumsd.javacore.chapter_18.iterator;

import java.util.ArrayList;

/*
page 595 foreach
 */
public class ForEach {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            arr.add(i);
        }

        for (int v : arr) {
            System.out.print(v + " ");
        }
        System.out.println();

        int sum = 0;
        for (int v : arr) {
            sum += v;
        }
        System.out.println("sum = " + sum);
    }
}
