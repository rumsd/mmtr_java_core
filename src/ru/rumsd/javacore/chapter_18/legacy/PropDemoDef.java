package ru.rumsd.javacore.chapter_18.legacy;

import java.util.Properties;
import java.util.Set;

/*
page 648-649 Использование Properties по умолчанию
 */
public class PropDemoDef {
    public static void main(String[] args) {

        Properties defList = new Properties();
        defList.put("pName1", "pValue1");
        defList.put("pName2", "pValue2");
        defList.put("pName10", "pValue10");

        Properties properties = new Properties(defList);
        properties.put("pName3", "pValue3");
        properties.put("pName4", "pValue4");
        properties.put("pName5", "pValue5");

        Set<?> keys = properties.keySet();

        for (Object key : keys) {
            System.out.println("key = " + key + ", value = " + properties.getProperty((String) key));
        }

        String str = properties.getProperty("pName10");
        System.out.println("pName10 = " + str);
    }
}
