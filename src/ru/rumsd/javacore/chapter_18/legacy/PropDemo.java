package ru.rumsd.javacore.chapter_18.legacy;

import java.util.Properties;
import java.util.Set;

/*
page 647-648 Использование Properties
 */
public class PropDemo {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put("pName1", "pValue1");
        properties.put("pName2", "pValue2");
        properties.put("pName3", "pValue3");
        properties.put("pName4", "pValue4");
        properties.put("pName5", "pValue5");

        Set<?> keys = properties.keySet();

        for (Object key : keys) {
            System.out.println("key = " + key + ", value = " + properties.getProperty((String) key));
        }

        String propNotFound = properties.getProperty("pName10", "не найдена");
        System.out.println("pName10 = " + propNotFound);
    }
}
