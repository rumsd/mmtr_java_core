package ru.rumsd.javacore.chapter_18.legacy;

import java.util.Hashtable;
import java.util.Iterator;


/*
page 645 Использование итератора в Hashtable
 */
public class HTDemo2 {
    public static void main(String[] args) {
        Hashtable<String, Double> balance = new Hashtable<>();
        balance.put("Person 1", 123.11);
        balance.put("Person 2", 111.11);
        balance.put("Person 3", 222.11);
        balance.put("Person 4", 333.11);
        balance.put("Person 5", 444.11);

        Iterator<String> iterator = balance.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.println(key + ": " + balance.get(key));
        }

        System.out.println();
        double bal = balance.get("Person 1");
        balance.put("Person 1", bal + 1000);
        System.out.println("new balance Person 1 = " + balance.get("Person 1"));
    }
}
