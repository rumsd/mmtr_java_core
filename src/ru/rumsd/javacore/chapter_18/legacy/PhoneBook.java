package ru.rumsd.javacore.chapter_18.legacy;

import java.io.*;
import java.util.Properties;

/*
page 650 база данных телефонов на Properties
 */
public class PhoneBook {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        String name, phone;
        FileInputStream fis = null;
        boolean changed = false;

        try {
            fis = new FileInputStream("phonebook.dat");
        } catch (FileNotFoundException e) {

        }
        try {
            if (fis != null) {
                properties.load(fis);
            }
        } catch (IOException e) {
            System.out.println("Ошибка чтения файла");
        }

        do {
            System.out.println("name = ");
            name = console.readLine();
            if (name.equals("exit")) continue;

            System.out.println("number = ");
            phone = console.readLine();
            properties.put(name, phone);
            changed = true;
        } while (!name.equals("exit"));

        if (changed) {
            FileOutputStream fos = new FileOutputStream("phonebook.dat");
            properties.store(fos, "Телефонная книга");
            fos.close();
        }
        do {
            System.out.println("Введите имя: ");
            name = console.readLine();
            if (name.equals("exit")) continue;
            System.out.println((String) properties.getProperty(name));
        } while (!name.equals("exit"));
    }
}
