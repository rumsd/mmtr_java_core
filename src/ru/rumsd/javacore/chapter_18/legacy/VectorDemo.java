package ru.rumsd.javacore.chapter_18.legacy;

import java.util.Enumeration;
import java.util.Vector;

/*
page 638 Операции с вектором
 */
public class VectorDemo {
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>(3, 2);
        System.out.println("Начальный размер вектора = " + vector.size());
        System.out.println("Начальная емкость вектора = " + vector.capacity());

        vector.add(1);
        vector.add(2);
        vector.add(3);
        vector.add(4);

        System.out.println("Емкость вектора после ввода 4х элементов");
        vector.add(5);
        System.out.println("Текущая емкость вектора = " + vector.capacity());
        vector.add(6);
        vector.add(7);
        System.out.println("Текущая емкость вектора = " + vector.capacity());
        vector.add(9);
        vector.add(10);
        System.out.println("Текущая емкость вектора = " + vector.capacity());
        vector.add(11);
        vector.add(12);
        System.out.println("Текущая емкость вектора = " + vector.capacity());

        System.out.println("first element = " + vector.firstElement());
        System.out.println("last element = " + vector.lastElement());
        if (vector.contains(3)) {
            System.out.println("vector содержит 3");
        }

        Enumeration<Integer> vEnum = vector.elements();
        System.out.println("Элементы вектора: ");
        while (vEnum.hasMoreElements()) {
            System.out.print(vEnum.nextElement() + " ");
        }
    }
}
